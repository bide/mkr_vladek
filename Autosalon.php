<?php

include 'Car.php';

class Autosalon
{
    private $cars;

    public function __construct()
    {
        $this->cars = array(
            new Car('Toyota', 'Rav4', 'hybrid', 5),
            new Car ('Toyota', 'Prius', 'electro', 5),
            new Car('Toyota', 'Sequoya', 'gas', 7),
        ) ;
    }


    public function addCar($brand, $model, $fuel, $passengers){
        $this->cars[] = new Car($brand, $model, $fuel, $passengers);
    }

    public function getCarsCountByBrand($brand){
        $count = 0;

        foreach ($this->cars as $car){
            if($car->getBrand() == $brand){
                 $count++;
            }
        }
        return $count;
    }

    public function getCarsWithPlaces($passengers_count){
        $cars = array();

        foreach ($this->cars as $car){
            if($car->getPassengers() >= $passengers_count){
                $cars[] = $car;
            }
        }
        return $cars;
    }


}