<?php

use \PHPUnit\Framework\TestCase;

include 'Autosalon.php';


class AutosalonTest extends TestCase
{

    private $auto_salon;

    protected function setUp() : void
    {
        $this->auto_salon = new Autosalon();
    }

    public function testCar()
    {
        $cars = $this->auto_salon->getCarsWithPlaces(5);

        $this->assertEquals(3, count($cars));
    }


    /**
     * @dataProvider addDataProvider
     * @param $brand
     * @param $expected
     */
    public function testBrandCount($brand, $expected)
    {
        $result = $this->auto_salon->getCarsCountByBrand($brand);
        $this->assertEquals($expected, $result);
    }


    public function addDataProvider() {
        return array(
            array('Toyota', 3),
            array('Toyota', 2),
            array('KIA', 0),
        );
    }


    protected function tearDown() : void
    {
        isset($this->auto_salon);
    }
}