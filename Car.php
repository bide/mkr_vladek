<?php


class Car
{
    private $brand;
    private $model;
    private $fuel;
    private $passengers;

    /**
     * Car constructor.
     * @param $brand
     * @param $model
     * @param $fuel
     * @param $passengers
     */
    public function __construct($brand, $model, $fuel, $passengers)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->fuel = $fuel;
        $this->passengers = $passengers;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param mixed $fuel
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;
    }

    /**
     * @return mixed
     */
    public function getPassengers()
    {
        return $this->passengers;
    }

    /**
     * @param mixed $passengers
     */
    public function setPassengers($passengers)
    {
        $this->passengers = $passengers;
    }
}